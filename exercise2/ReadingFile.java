package exercise2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ReadingFile {
    public static void main(String[] args) throws IOException {
        Path filePath = Paths.get("./resources/data.txt");
//        List<String> lines = Files.readAllLines(filePath);
//        System.out.println(lines);

        Files.lines(filePath)
                .map(String::toLowerCase)
                .filter(str -> str.contains("t"))
                .forEach(System.out::println); // process one at a line
    }
}
